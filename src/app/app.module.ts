import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NdbxIconModule } from '@allianz/ngx-ndbx/icon';
import { NxIconModule } from '@aposin/ng-aquila/icon';
import { NxPopoverModule } from '@aposin/ng-aquila/popover';
import { NxButtonModule } from '@aposin/ng-aquila/button';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NdbxIconModule,
    NxIconModule,
    NxPopoverModule,
    NxButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
